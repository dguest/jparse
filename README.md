Example using nlohmann::json
============================

To use:
- clone this project into `jparse`
- `mkdir build`
- `cd build`
- `cmake ../jparse/src`
- `make`
- `./main ../jparse/data/example.json`
