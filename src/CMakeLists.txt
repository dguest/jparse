cmake_minimum_required(VERSION 3.22)
project(jcheck)
find_package(nlohmann_json 3.10.5 REQUIRED)

add_executable(main main.cxx)
target_link_libraries(main PRIVATE nlohmann_json::nlohmann_json)
set_target_properties(main PROPERTIES CXX_STANDARD 17)
