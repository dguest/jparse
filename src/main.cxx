#include <iostream>
#include <fstream>
#include <map>
#include "nlohmann/json.hpp"

int main(int narg, char* argv[]) {
  if (narg != 2) {
    std::cerr << "usage:" << argv[0] << ": <json-file>" << std::endl;
    return 1;
  }
  // nlohmann can read from strings, or input streams
  std::ifstream in(argv[1]);
  // build the json object
  auto json = nlohmann::json::parse(in);
  // Get the std::map. The `get` call will try to cast the json into a
  // c++ object.
  std::map<float, float> map = json.get<std::map<float, float>>();
  // loop over the map and print it out
  for (auto [v1, v2]: map) {
    std::cout << v1 << " " << v2 << std::endl;
  }
  return 0;
}
